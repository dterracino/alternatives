﻿namespace Alternatives.UnitTest.TestModel.Implementations
{
   public  class AnotherTestGenericInterface : IGenericInterface<string>
    {
        public string GenericField { get; set; }
    }
}
