﻿namespace Alternatives.UnitTest.TestModel.Implementations
{
    public class TestGenericInterface : IGenericInterface<int>
    {
        public int GenericField { get; set ; }
    }
}