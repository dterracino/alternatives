﻿namespace Alternatives.UnitTest.TestModel
{
    public interface IGenericInterface <T>
    {
        T GenericField { get; set; }
    }
}
