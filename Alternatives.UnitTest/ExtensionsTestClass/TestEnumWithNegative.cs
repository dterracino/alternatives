﻿namespace Alternatives.UnitTest.ExtensionsTestClass
{
    public enum TestEnumWithNegative
    {
        TestValueNegative1 = -1,
        TestValue0 = 0,
        TestValue1 = 1
    }
}